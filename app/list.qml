import QtQuick 2.15
import net.davidedmundson.private.todolist 0.1
import QtQuick.Controls 2.4 as Controls
import QtQuick.Layouts 1.1

import org.kde.kirigami 2.10 as Kirigami
import org.kde.kitemmodels 1.0 as KItemModels

Kirigami.ApplicationWindow {

    globalDrawer: Kirigami.GlobalDrawer {
        id: contextDrawer
    }

    pageStack.initialPage: Kirigami.ScrollablePage {
        id: page
        property  bool hideCompleted: false

        contextualActions: [
            Kirigami.Action {
                iconName: "edit"
                text: page.hideCompleted ? "Show Completed" : "Hide Completed"
                onTriggered: {
                    page.hideCompleted = !page.hideCompleted
                }
            }]

        ListView {
            anchors.fill: parent

            model: TodoSortFilterModel {
                hideCompleted: page.hideCompleted
                sourceModel: todoModel
            }

            TodoModel {
                id: todoModel
                path: "/home/david/.todos/todo-app.ics"
            }

            delegate: Kirigami.SwipeListItem {
                contentItem: RowLayout {
                    id: listEntry
                    property bool editing: false

                    Controls.CheckBox {
                        id: checkbox
                        checked: model.Completed
                        onToggled:  model.Completed = checked
                    }
                    Kirigami.Icon {
                        source: model.Priority == 0 ? "non-starred-symbolic" : "starred-symbolic"
                        Layout.preferredWidth: checkbox.implicitWidth
                        Layout.preferredHeight: checkbox.implicitHeight

                        MouseArea {
                            anchors.fill: parent
                            onClicked: model.Priority === 0 ? model.Priority = 10 : model.Priority = 0
                        }

                    }
                    Controls.Label {
                        Layout.fillWidth: true
                        text: model.Summary
                        visible: !listEntry.editing
                        elide: Text.ElideRight
                    }
                    Controls.TextField {
                        id: textField
                        Layout.fillWidth: true
                        text: model.Summary
                        visible: listEntry.editing
                        onAccepted: {
                            model.Summary = text;
                            listEntry.editing = false;
                        }
                    }
                }
                actions: [
                    Kirigami.Action {
                        icon.name: "list-remove"
                        onTriggered: todoModel.removeRow(model.index)
                    },
                    Kirigami.Action {
                        icon.name: "edit-entry"
                        onTriggered: {
                            listEntry.editing = !listEntry.editing
                            if (listEntry.editing) {
                                listEntry.textField.focus = true;
                            }
                        }
                    }
                ]
            }
            footerPositioning: ListView.OverlayFooter
            footer:
                RowLayout {
                id: footer
                width: parent.width
                z: 10 // DAVE - talk to marco about why we need this, it seems wrong to me?

                function submit() {
                    if (quickAddTextField.text.length == 0) {
                        return;
                    }
                    todoModel.addRow(quickAddTextField.text)
                    quickAddTextField.clear();
                }

                Controls.TextField {
                    id: quickAddTextField
                    Layout.fillWidth: true
                    placeholderText: "New Todo"

                    onAccepted:{
                        footer.submit()
                    }
                }
                Controls.Button {
                    text: "+"
                    onClicked: footer.submit()
                }
            }
        }
    }
}
