#include <QApplication>
#include <QQmlApplicationEngine>

int main(int argc, char **argv)
{
    QApplication app(argc, argv);

    QQmlApplicationEngine view;
    view.load(QUrl(QStringLiteral("/home/david/projects/temp/davetodolist/app/list.qml")));

    app.exec();
}
