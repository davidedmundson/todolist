This is the start of a TODO list application.

It's written for my needs but it can be forked or whatever.

Plans are to make a small import that can power a plasmoid with a config file to choose a ics file
and maybe a Kirigami app that we can use on the phone where the global drawer could be a list of calendars and we can have a full edit UI in a page.

It's backed by ICS files and KCalendarCore though my personal usage is to just have a simple list.

As we use KCalendarCore's classes someone could add remote syncing or google or whatever.

Personally I've used vdirsync to sync to nextcloud.
