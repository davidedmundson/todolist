#include "todoplugin.h"
#include "todomodel.h"

#include <QQmlEngine>

void TodoPlugin::registerTypes (const char *uri)
{
    qmlRegisterType<FileBackedTodoModel>(uri, 0, 1, "TodoModel");
    qmlRegisterType<TodoSortFilterModel>(uri, 0, 1, "TodoSortFilterModel");
}

