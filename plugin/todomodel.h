#pragma once

#include <KCalCore/Todo>
#include <KCalCore/Calendar>

#include <KCalCore/FileStorage>

#include <QAbstractListModel>
#include <QSortFilterProxyModel>

#include <QTimer>

class TodoModel: public QAbstractListModel
{
    Q_OBJECT
public:
    enum Roles {
        Summary = Qt::DisplayRole,
        Completed = Qt::UserRole,
        CreatedDate,
        StartDate,
        DueDate,
        CompletedDate,
        ModifiedDate,
        Description,
        Categories,  // as a stringlist
        Priority,
    };
    Q_ENUM(Roles)

    TodoModel(QObject *parent = nullptr);
    int rowCount(const QModelIndex &parent=QModelIndex()) const override;
    QVariant data(const QModelIndex &index, int role) const override;
    bool setData(const QModelIndex &index, const QVariant &value, int role) override;
    QHash<int, QByteArray> roleNames() const override;

    /**
     * @brief addRow
     * It then returns a row so one can call setData on it
     * with remaining properties
     */
    Q_INVOKABLE int addRow(const QString &summary);
    Q_INVOKABLE void removeRow(int row);

    void setCalendar(KCalendarCore::Calendar::Ptr);

    protected:
    KCalendarCore::Calendar::Ptr m_calendar;
    KCalendarCore::Todo::List m_todos;
};

class TodoSortFilterModel: public QSortFilterProxyModel
{
    Q_OBJECT
    Q_PROPERTY(bool hideCompleted MEMBER m_hideCompleted WRITE setHideCompleted NOTIFY hideCompletedChanged)

public:
    TodoSortFilterModel(QObject *parent = nullptr);
    void setHideCompleted(bool hideCompleted);
Q_SIGNALS:
    void hideCompletedChanged();
protected:
    bool filterAcceptsRow(int source_row, const QModelIndex &source_parent) const override;
    bool lessThan(const QModelIndex &source_left, const QModelIndex &source_right) const override;
private:
    bool m_hideCompleted = false;
};



class FileBackedTodoModel : public TodoModel, private KCalendarCore::Calendar::CalendarObserver
{
    Q_OBJECT
    Q_PROPERTY(QString path MEMBER m_path WRITE setPath NOTIFY pathChanged)
    // Some sort of Status enum here...
public:
    FileBackedTodoModel(QObject *parent = nullptr);
    ~FileBackedTodoModel();
    // path to ICS file
    void setPath(const QString &path);

    void calendarModified(bool modified, KCalendarCore::Calendar *calendar) override;
    void calendarIncidenceAdded(const KCalendarCore::Incidence::Ptr &incidence) override;
    void calendarIncidenceChanged(const KCalendarCore::Incidence::Ptr &incidence) override;
    void calendarIncidenceDeleted(const KCalendarCore::Incidence::Ptr &incidence, const KCalendarCore::Calendar *calendar) override;

public Q_SLOTS:
    void queueSave();
    void save();
Q_SIGNALS:
    void pathChanged();
private:
    void reload();
    void load();

    QString m_path;
    QScopedPointer<KCalendarCore::FileStorage> m_storage;
    bool m_saving = false;
    QTimer m_saveTimer;
};
