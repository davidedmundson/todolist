#include "todomodel.h"

#include <KCalCore/FileStorage>
#include <KCalCore/MemoryCalendar>
#include <QCalendar>
#include <QDebug>
#include <QMetaEnum>

#include <KDirWatch>
#include <QTimer>
#include <QCoreApplication>

using namespace KCalendarCore;

TodoModel::TodoModel(QObject *parent)
    : QAbstractListModel(parent)
{}

int TodoModel::rowCount(const QModelIndex &parent) const
{
    if (parent.isValid()) {
        return 0;
    }
    return m_todos.count();
}

QVariant TodoModel::data(const QModelIndex &index, int role) const
{
    if (!checkIndex(index, QAbstractItemModel::CheckIndexOption::IndexIsValid)) {
        return QVariant();
    }
    switch (role) {
    case TodoModel::Summary:
        return m_todos[index.row()]->summary();
    case TodoModel::Completed:
        return m_todos[index.row()]->isCompleted();
    case TodoModel::CreatedDate:
        return m_todos[index.row()]->created();
    case TodoModel::StartDate:
        return m_todos[index.row()]->dtStart();
    case TodoModel::DueDate:
        return m_todos[index.row()]->dtDue();
    case TodoModel::CompletedDate:
        return m_todos[index.row()]->completed();
    case TodoModel::Description:
        return m_todos[index.row()]->description();
    case TodoModel::ModifiedDate:
        return m_todos[index.row()]->lastModified();
    case TodoModel::Categories:
        return m_todos[index.row()]->categories();
    case TodoModel::Priority:
        return m_todos[index.row()]->priority();
    }

    return QVariant();
}

bool TodoModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if (!checkIndex(index, QAbstractItemModel::CheckIndexOption::IndexIsValid)) {
        return false;
    }
    switch (role) {
    case TodoModel::Summary:
        m_todos[index.row()]->setSummary(value.toString());
        break;
    case TodoModel::Completed:
        m_todos[index.row()]->setCompleted(value.toBool());
        break;
    case TodoModel::StartDate:
        m_todos[index.row()]->setDtStart(value.toDateTime());
        break;
    case TodoModel::DueDate:
        m_todos[index.row()]->setDtDue(value.toDateTime());
        break;
    case TodoModel::Description:
        m_todos[index.row()]->setDescription(value.toString());
        break;
    case TodoModel::Categories:
        m_todos[index.row()]->setCategories(value.toStringList());
        break;
    case TodoModel::Priority:
        qDebug() << "Setting priority to " << value.toInt();
        m_todos[index.row()]->setPriority(value.toInt());
        break;
    }

    emit dataChanged(index, index);

    return true;
}

QHash<int, QByteArray> TodoModel::roleNames() const
{
    QHash<int, QByteArray> roles = QAbstractItemModel::roleNames();

    QMetaEnum e = metaObject()->enumerator(metaObject()->indexOfEnumerator("Roles"));

    for (int i = 0; i < e.keyCount(); ++i) {
        qDebug() << e.value(i);
        roles.insert(e.value(i), e.key(i));
    }
    return roles;
}

int TodoModel::addRow(const QString &summary)
{
    Todo::Ptr todo(new Todo());
    todo->setSummary(summary);
    if (!m_calendar->addTodo(todo)) {
        return -1;
    }
    int row = rowCount();
    beginInsertRows(QModelIndex(), row, row);
    m_todos.append(todo);
    endInsertRows();
    return row;
}

void TodoModel::removeRow(int row)
{
    if (row < 0 || row > m_todos.count()) {
        return;
    }
    beginRemoveRows(QModelIndex(), row, row);
    m_calendar->deleteTodo(m_todos.takeAt(row));
    endRemoveRows();
}

void TodoModel::setCalendar(Calendar::Ptr calendar)
{
    m_calendar = calendar;
    beginResetModel();
    m_todos = calendar->todos();
    endResetModel();
}

FileBackedTodoModel::FileBackedTodoModel(QObject *parent)
    : TodoModel(parent)
{
    connect(&m_saveTimer, &QTimer::timeout, this, &FileBackedTodoModel::save);
    m_saveTimer.setInterval(1000);
    m_saveTimer.setSingleShot(true);

    connect(KDirWatch::self(), &KDirWatch::created, this, [this](const QString &path) {
        if (path != m_path) {
            return;
        }
        reload();
    });

    connect(KDirWatch::self(), &KDirWatch::dirty, this, [this](const QString &path) {
        if (path != m_path) {
            return;
        }
        reload();
    });
}

FileBackedTodoModel::~FileBackedTodoModel()
{
    if (m_saveTimer.isActive()) {
        save();
    }
}

void FileBackedTodoModel::setPath(const QString &path)
{
    if (!m_path.isEmpty()) {
        qDebug() << "BOOOO - path can currently only be set once";
        return;
    }
    m_path = path;
    m_calendar.reset(new MemoryCalendar(QTimeZone()));
    m_storage.reset(new FileStorage(m_calendar, path));

    reload();

    KDirWatch::self()->addFile(m_path);

    emit pathChanged();
}

void FileBackedTodoModel::reload()
{
    //ignore updates whilst we're loading
    m_calendar->unregisterObserver(this);
    m_calendar->close();
    bool rc = m_storage->load();
    if (!rc) {
        qWarning() << "Failed to load file" << m_path;
    }
    setCalendar(m_calendar);
    m_calendar->registerObserver(this);
}

void FileBackedTodoModel::queueSave()
{
    // ignore updates whilst saving or loading, they just confuse things
    if (m_saving) {
        return;
    }
    m_saveTimer.start();
}

void FileBackedTodoModel::calendarModified(bool modified, KCalendarCore::Calendar *calendar)
{
    qDebug();
    queueSave();
}

void FileBackedTodoModel::calendarIncidenceAdded(const Incidence::Ptr &incidence)
{
    qDebug() << "ADDED";
}

void FileBackedTodoModel::calendarIncidenceChanged(const KCalendarCore::Incidence::Ptr &incidence)
{
    qDebug() << "Changed";
    //apparently not covered by "modified"
    queueSave();
}

void FileBackedTodoModel::calendarIncidenceDeleted(const Incidence::Ptr &incidence, const Calendar *calendar)
{
    qDebug() << "incident deleted";
    //apparently not covered by "modified"
    queueSave();
}

void FileBackedTodoModel::save()
{
    qDebug() << "SAVE";
    KDirWatch::self()->removeFile(m_path);
    m_saving = true;

    m_storage->save();

    m_saving = false;
    KDirWatch::self()->addFile(m_path);
}

TodoSortFilterModel::TodoSortFilterModel(QObject *parent)
    : QSortFilterProxyModel(parent)
{
}

void TodoSortFilterModel::setHideCompleted(bool hideCompleted)
{
    if (m_hideCompleted == hideCompleted) {
        return;
    }
    m_hideCompleted = hideCompleted;
    invalidateFilter();
    emit hideCompletedChanged();
}

bool TodoSortFilterModel::filterAcceptsRow(int source_row, const QModelIndex &source_parent) const
{
    QModelIndex index = sourceModel()->index(source_row, 0, source_parent);
    if (m_hideCompleted && index.data(TodoModel::Completed).toBool()) {
        return false;
    }
    return true;
}

bool TodoSortFilterModel::lessThan(const QModelIndex &source_left, const QModelIndex &source_right) const
{
    return false;
}


